// ==================================================
// プレイヤーのオブジェクト
// 
// 
// ゲームに登場する「プレイヤー」「ウキ」に関するコードを定義する。
// 各オブジェクトはフィールド変数とメソッドを持つ。
// ==================================================



// ==================================================
// プレイヤー
// ==================================================
var player = function(power) {
	// html用ID
	this.id = "player";
	// x座標
	this.x = RANGE_WINDOW_WIDTH / 2;
	// y座標
	this.y = RANGE_WINDOW_HEIGHT;
	// 巻き取る力
	this.power = power;
	// 角度
	this.kakudo = 0;
	
	
	// クリック時の処理
	this.update_by_click = function(fase, uki, fish_array, x, y) {
		switch (fase.value) {
			case "BEFORE_CAST":
				// 画面外をクリックした場合は何もしない
				if (RANGE_WINDOW_WIDTH < x || 0 > x) {
					break;
				}
				if (RANGE_WINDOW_HEIGHT < y || 0 > y) {
					break;
				}
				
				// クリックされた場所にウキを移動
				uki.x = x;
				uki.y = y;
				
				fase.value = "AFTER_CAST";
				
				break;
			case "AFTER_CAST":
				IS_PAUSE = true;
				fase.value = "DIALOG_RECAST";
				
				break;
			case "FIGHTING":
				var fish = get_is_fight_fish(fish_array);
				
				// 魚とウキを近づける
				var next_x = get_next_point(uki.x, this.x, this.power);
				var next_y = get_next_point(uki.y, this.y, this.power);
				
				uki.x = next_x;
				uki.y = next_y;
				fish.x = next_x;
				fish.y = next_y;
				
				// 最後まで巻き取った場合
				if (fish.x == this.x && fish.y == this.y) {
					fase.value = "RESULT";
					fish.is_get = true;
				}
				
				break;
			default:
				// 何もしない
				
		}
	}
}

// ==================================================
// ウキ
// ==================================================
var uki = function(x, y) {
	// html用ID
	this.id = "uki";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 16;
	// 画像
	this.bg_img = "";
	// 角度
	this.kakudo = 0;
	
	// 1フレーム毎の処理
	this.update = function(fase) {
		switch (fase.value) {
			case "FIGHTING":
				this.bg_img = "./img/uki/uki_1_fight.gif";
				
				break;
			default:
				this.bg_img = "./img/uki/uki_1_water.png";
				
		}
	}
}

