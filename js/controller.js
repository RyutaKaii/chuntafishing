// ==================================================
// コントローラー
// 
// 
// 一定時間ごとに下記の処理を繰り返す。
//   ・オブジェクトの状態を更新
//   ・描画
// フェーズで状態を管理する。
// 
// ==========【基本フロー】==========
//     [BEFORE_CAST]
//         ↓ウキ投入
//     [AFTER_CAST]
//         ↓魚がエサを食べる
//     [FIGHTING]
//         ↓岸まで巻き取る
//     [RESULT]
//         ↓続けるボタン
//     [BEFORE_CAST]
// 
// ==========【魚に逃げられる】==========
//     [BEFORE_CAST]
//         ↓ウキ投入
//     [AFTER_CAST]
//         ↓魚がエサを食べる
//     [FIGHTING]
//         ↓魚に対岸まで逃げられる
//     [BEFORE_CAST]
// 
// ==========【投げなおし】==========
//     [BEFORE_CAST]
//         ↓ウキ投入
//     [AFTER_CAST]
//         ↓どこかをクリック
//     [DIALOG_RECAST]
//         ↓なげなおし選択    ↑キャンセル
//     [BEFORE_CAST]
// 
// ==========【時間切れ】==========
//     [XXX]
//         ↓時間切れ[
//     [END]
// ==================================================



// ==================================================
// 定数
// ==================================================
// 背景画像
var BGIMG_SRC = "./img/bg/bg.jpg";

// 初期スコア
var START_SCORE = 0;

// 初期時間(ms)
var START_TIME = 45000;

// 1フレームあたりの時間(ms)
var FRAME_TIME = 50;

// プレイヤーの巻取り力
var PLAYER_POWER = 50;

// ウインドウ横幅
var RANGE_WINDOW_WIDTH = 700;

// ウインドウ縦幅
var RANGE_WINDOW_HEIGHT = 500;

// 描画ウインドウのID
var NAME_ID_DISP = "panel_dispwindow";

// 説明ウインドウのID
var NAME_ID_INTRO = "panel_intro";



// ==================================================
// グローバル変数
// ==================================================
// システム
var SYSTEM = null;

// クリック情報
var CLICK_INFO = null;

// ポーズしているか
var IS_PAUSE = true;



// ==================================================
// 読込時の処理
// ==================================================
function load_game() {
	SYSTEM = new system(START_SCORE, START_TIME, FRAME_TIME, PLAYER_POWER);
	CLICK_INFO = new click_info();
	
	// 魚を設定
	var fish_array = new Array();
	fish_array.push( new fish_aji( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_ankou( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_ebi( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_fugu( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_fugu( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_fugu( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_hirame( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_ika( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_kajiki( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_manbou( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_same( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_tai( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_tako( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_tatsu( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	fish_array.push( new fish_takata( calc_ramdam_value(0, RANGE_WINDOW_WIDTH), calc_ramdam_value(0, RANGE_WINDOW_HEIGHT) ) );
	SYSTEM.set_fish_array(fish_array);
	
	// クリック時の処理
	document.onmousedown = function(e) {
		CLICK_INFO.set_value(true, e.pageX, e.pageY);
	}
}

// ==================================================
// ゲームを開始
// ==================================================
function start_game() {
	// 説明パネルを消す
	delete_view_element(NAME_ID_INTRO);
	
	view_set_window();
	view_background(BGIMG_SRC);
	
	CLICK_INFO.reset();
	
	// 初回のみ表示する説明用ダイアログを設定
	SYSTEM.start();
	
	// 一定間隔で実行
	setInterval(function(){exec_one_frame()}, FRAME_TIME);
}

// ==================================================
// 1フレームごとの処理
// ==================================================
function exec_one_frame() {
	// ポーズ時は処理を実行しない
	if (IS_PAUSE) {
		return;
	}
	
	SYSTEM.update();
	SYSTEM.view();
	SYSTEM.finalize();
}

// ==================================================
// ウインドウ設定
// ==================================================
function view_set_window() {
	document.getElementById(NAME_ID_DISP).style.width = String(RANGE_WINDOW_WIDTH) + "px";
	document.getElementById(NAME_ID_DISP).style.height = String(RANGE_WINDOW_HEIGHT) + "px";
}

// ==================================================
// 背景を描画
// ==================================================
function view_background(img) {
	document.getElementById(NAME_ID_DISP).style.backgroundImage  = "url('" + img + "')";
}

// ==================================================
// 初回説明確認ボタンを押した際の処理
// ==================================================
function on_click_dialog_start() {
	CLICK_INFO.reset();
	
	IS_PAUSE = false;
}

// ==================================================
// 投げなおし確認ボタンyesを押した際の処理
// ==================================================
function on_click_dialog_yes() {
	SYSTEM.fase.value = "BEFORE_CAST";
	
	CLICK_INFO.reset();
	
	IS_PAUSE = false;
}

// ==================================================
// 投げなおし確認ボタンnoを押した際の処理
// ==================================================
function on_click_dialog_no() {
	SYSTEM.fase.value = "AFTER_CAST";
	
	CLICK_INFO.reset();
	
	IS_PAUSE = false;
}

// ==================================================
// 結果画面のボタンを押した際の処理
// ==================================================
function on_click_btn_result() {
	SYSTEM.fase.value = "BEFORE_CAST";
	
	CLICK_INFO.reset();
	
	IS_PAUSE = false;
}

// ==================================================
// 終了画面のボタンを押した際の処理
// ==================================================
function on_click_btn_end() {
	location.reload();
}

