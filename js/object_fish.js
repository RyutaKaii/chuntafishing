// ==================================================
// 魚のオブジェクト
// 
// 
// ゲームに登場する「魚」に関するコードを定義する。
// 各オブジェクトはフィールド変数とメソッドを持つ。
// ==================================================



// ==================================================
// あじ
// ==================================================
var fish_aji = function(x, y) {
	// html用ID
	this.id = "fish_aji" + "_" + get_random_str();
	// 名前
	this.name = "あじ";
	// 水中画像
	this.bg_img = "./img/fish/fish_s.png";
	// 結果画像
	this.result_img = "./img/result/aji.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 8;
	// 速度
	this.speed = 5;
	// 対岸に逃げる力
	this.power = 2.5;
	// スコア
	this.score = 100;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// アンコウ
// ==================================================
var fish_ankou = function(x, y) {
	// html用ID
	this.id = "fish_ankou" + "_" + get_random_str();
	// 名前
	this.name = "あんこう";
	// 水中画像
	this.bg_img = "./img/fish/fish_s.png";
	// 結果画像
	this.result_img = "./img/result/ankou.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 8;
	// 速度
	this.speed = 4;
	// 対岸に逃げる力
	this.power = 3;
	// スコア
	this.score = 200;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// エビ
// ==================================================
var fish_ebi = function(x, y) {
	// html用ID
	this.id = "fish_ebi" + "_" + get_random_str();
	// 名前
	this.name = "エビ";
	// 水中画像
	this.bg_img = "./img/fish/fish_s.png";
	// 結果画像
	this.result_img = "./img/result/ebi.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 8;
	// 速度
	this.speed = 2;
	// 対岸に逃げる力
	this.power = 1;
	// スコア
	this.score = 50;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// フグ
// ==================================================
var fish_fugu = function(x, y) {
	// html用ID
	this.id = "fish_fugu" + "_" + get_random_str();
	// 名前
	this.name = "フグ";
	// 水中画像
	this.bg_img = "./img/fish/fish_s.png";
	// 結果画像
	this.result_img = "./img/result/fugu.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 8;
	// 速度
	this.speed = 4;
	// 対岸に逃げる力
	this.power = 2;
	// スコア
	this.score = 50;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// タツノオトシゴ
// ==================================================
var fish_tatsu = function(x, y) {
	// html用ID
	this.id = "fish_tatsu" + "_" + get_random_str();
	// 名前
	this.name = "タツノオトシゴ";
	// 水中画像
	this.bg_img = "./img/fish/fish_s.png";
	// 結果画像
	this.result_img = "./img/result/tatsu.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 8;
	// 速度
	this.speed = 5;
	// 対岸に逃げる力
	this.power = 4;
	// スコア
	this.score = 300;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// ヒラメ
// ==================================================
var fish_hirame = function(x, y) {
	// html用ID
	this.id = "fish_hirame" + "_" + get_random_str();
	// 名前
	this.name = "ヒラメ";
	// 水中画像
	this.bg_img = "./img/fish/fish_m.png";
	// 結果画像
	this.result_img = "./img/result/hirame.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 16;
	// 速度
	this.speed = 5;
	// 対岸に逃げる力
	this.power = 5;
	// スコア
	this.score = 500;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// イカ
// ==================================================
var fish_ika = function(x, y) {
	// html用ID
	this.id = "fish_ika" + "_" + get_random_str();
	// 名前
	this.name = "イカ";
	// 水中画像
	this.bg_img = "./img/fish/fish_m.png";
	// 結果画像
	this.result_img = "./img/result/ika.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 16;
	// 速度
	this.speed = 5;
	// 対岸に逃げる力
	this.power = 6;
	// スコア
	this.score = 400;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// タイ
// ==================================================
var fish_tai = function(x, y) {
	// html用ID
	this.id = "fish_tai" + "_" + get_random_str();
	// 名前
	this.name = "タイ";
	// 水中画像
	this.bg_img = "./img/fish/fish_m.png";
	// 結果画像
	this.result_img = "./img/result/tai.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 16;
	// 速度
	this.speed = 7;
	// 対岸に逃げる力
	this.power = 7;
	// スコア
	this.score = 700;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// タコ
// ==================================================
var fish_tako = function(x, y) {
	// html用ID
	this.id = "fish_ankou" + "_" + get_random_str();
	// 名前
	this.name = "タコ";
	// 水中画像
	this.bg_img = "./img/fish/fish_m.png";
	// 結果画像
	this.result_img = "./img/result/tako.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 16;
	// 速度
	this.speed = 5;
	// 対岸に逃げる力
	this.power = 7;
	// スコア
	this.score = 500;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// カジキ
// ==================================================
var fish_kajiki = function(x, y) {
	// html用ID
	this.id = "fish_kajiki" + "_" + get_random_str();
	// 名前
	this.name = "カジキ";
	// 水中画像
	this.bg_img = "./img/fish/fish_l.png";
	// 結果画像
	this.result_img = "./img/result/kajiki.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 32;
	// 速度
	this.speed = 5;
	// 対岸に逃げる力
	this.power = 8;
	// スコア
	this.score = 1000;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}
// ==================================================
// マンボウ
// ==================================================
var fish_manbou = function(x, y) {
	// html用ID
	this.id = "fish_manbou" + "_" + get_random_str();
	// 名前
	this.name = "マンボウ";
	// 水中画像
	this.bg_img = "./img/fish/fish_l.png";
	// 結果画像
	this.result_img = "./img/result/manbou.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 32;
	// 速度
	this.speed = 3;
	// 対岸に逃げる力
	this.power = 6;
	// スコア
	this.score = 1000;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// サメ
// ==================================================
var fish_same = function(x, y) {
	// html用ID
	this.id = "fish_same" + "_" + get_random_str();
	// 名前
	this.name = "サメ";
	// 水中画像
	this.bg_img = "./img/fish/fish_l.png";
	// 結果画像
	this.result_img = "./img/result/same.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 32;
	// 速度
	this.speed = 5;
	// 対岸に逃げる力
	this.power = 10;
	// スコア
	this.score = 3000;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}

// ==================================================
// たかた
// ==================================================
var fish_takata = function(x, y) {
	// html用ID
	this.id = "fish_takata" + "_" + get_random_str();
	// 名前
	this.name = "たかた";
	// 水中画像
	this.bg_img = "./img/fish/fish_l.png";
	// 結果画像
	this.result_img = "./img/result/takata.png";
	// x座標
	this.x = x;
	// y座標
	this.y = y;
	// 中央からの距離
	this.range = 32;
	// 速度
	this.speed = 5;
	// 対岸に逃げる力
	this.power = 13;
	// スコア
	this.score = 1;
	// 角度
	this.kakudo = 0;
	
	// 戦っているかどうか
	this.is_fight = false;
	// 釣られたかどうか
	this.is_get = false;
	
	// 目標点x
	this.to_x = 0;
	// 目標点y
	this.to_y = 0;
	// 目標に到着したか
	this.is_arrive = true;
	
	
	// 1フレーム毎の処理
	this.update = function(fase, uki) {
		fish_update(this, fase, uki);
	}
	
	// 自由に泳ぐ
	this.swim_free = function() {
		fish_swim_free(this);
	}
}





// ==================================================
// 魚の1フレームごとの処理
// ==================================================
function fish_update(self, fase, uki) {
	switch (fase.value) {
		case "BEFORE_CAST":
			self.swim_free();
			
			break;
		case "AFTER_CAST":
			// ウキに近づく
			self.to_x = uki.x;
			self.to_y = uki.y;
			
			self.x = get_next_point(self.x, self.to_x, self.speed);
			self.y = get_next_point(self.y, self.to_y, self.speed);
			
			// ウキにたどり着いた場合
			if (self.x == self.to_x && self.y == self.to_y) {
				self.is_fight = true;
				fase.value = "FIGHTING";
			}
			
			break;
		case "FIGHTING":
			if (true == self.is_fight) {
				// 釣られている場合は対岸に向かう
				self.to_x = calc_ramdam_value(0, RANGE_WINDOW_WIDTH);
				self.to_y = 0;
				
				self.x = get_next_point(self.x, self.to_x, self.power);
				self.y = get_next_point(self.y, self.to_y, self.power);
				uki.x = self.x;
				uki.y = self.y;
				
				// 対岸にたどり着いた場合
				if (self.y == self.to_y) {
					self.is_fight = false;
					fase.value = "BEFORE_CAST";
				}
			} else {
				// 釣られていない場合は自由に泳ぐ
				self.swim_free();
			}
			
			break;
		default:
			self.swim_free();
	}
		
	self.kakudo = get_kakudo(self.x, self.y, self.to_x, self.to_y);
}

// ==================================================
// 魚の自由に泳ぐ処理
// ==================================================
function fish_swim_free(self) {
	if (true == self.is_arrive) {
		self.is_arrive = false;
		
		// 目標点を決める
		self.to_x = calc_ramdam_value(0, RANGE_WINDOW_WIDTH - self.range * 2);
		self.to_y = calc_ramdam_value(0, RANGE_WINDOW_HEIGHT - self.range * 2);
	}
	
	// 目標に向かって進む
	self.x = get_next_point(self.x, self.to_x, self.speed);
	self.y = get_next_point(self.y, self.to_y, self.speed);
	
	// 目標に到着した場合
	if (self.x == self.to_x && self.y == self.to_y) {
		self.is_arrive = true;
	}
}

