// ==================================================
// 汎用機能
// 
// 
// 汎用的に利用するコードを定義する。
// ==================================================



// ==================================================
// ランダム値を算出
// min～maxまでの整数を返却
// ==================================================
function calc_ramdam_value(min, max) {
	return Math.floor( Math.random() * (max - min) );
}

// ==================================================
// ランダム文字列を取得
// ==================================================
function get_random_str() {
	return Math.random().toString(32).substring(2);
}

// ==================================================
// 次の位置を取得
// ==================================================
function get_next_point(now, to, speed) {
	// 既に目的地に到達している場合
	if (0 == to - now) {
		return to;
	}
	
	var next = 0;
	
	if (0 < to - now) {
		// プラス方向への移動の場合
		next = now + speed;
		
		if (next > to) {
			return to;
		}
		
		return next;
	} else {
		// マイナス方向への移動の場合
		next = now - speed;
		
		if (to > next) {
			return to;
		}
		
		return next;
	}
}

// ==================================================
// 戦っている魚を取得
// ==================================================
function get_is_fight_fish(fish_array) {
	for (var i = 0;i < fish_array.length; i++) {
		if (true == fish_array[i].is_fight) {
			return fish_array[i];
		}
	}
	
	return null;
}

// ==================================================
// 釣られた魚を取得
// ==================================================
function get_is_get_fish(fish_array) {
	for (var i = 0;i < fish_array.length; i++) {
		if (true == fish_array[i].is_get) {
			return fish_array[i];
		}
	}
	
	return null;
}

// ==================================================
// 指定されたシステムオブジェクト要素を削除
// ==================================================
function delete_system_element(id, array) {
	var delete_index = -1;
	
	for (var i = 0;i < array.length; i++) {
		if (id == array[i].id) {
			delete_index = i;
			break;
		}
	}
	
	if (-1 == delete_index) {
		return;
	}
	
	array.splice(delete_index , 1);
}

// ==================================================
// 指定された描画要素を削除
// ==================================================
function delete_view_element(id) {
	// 指定されたidの要素を削除
	if (document.getElementById(id) != null) {
		var element = document.getElementById(id);
		element.parentNode.removeChild(element);
	}
}

// ==================================================
// 角度を算出
// ==================================================
function get_kakudo(from_x, from_y, to_x, to_y) {
	var radian = Math.atan2( to_y - from_y, to_x - from_x );
	
	// 右方向が0であり、上を0としたいので-90する
	return radian * ( 180 / Math.PI ) - 90;
}

// ==================================================
// 0埋め文字列取得
// ==================================================
function get_zeroume_str(num, keta_su) {
	var num_str = String(num);
	
	var zero_su = keta_su - num_str.length;
	
	if (keta_su < num_str.length) {
		return num_str;
	}
	
	for (var i = 0;i < zero_su; i++) {
		num_str = "0" + num_str;
	}
	
	return num_str;
}

