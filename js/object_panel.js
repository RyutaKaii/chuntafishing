// ==================================================
// パネル
// 
// 
// ゲームに登場する「パネル」に関するコードを定義する。
// 各オブジェクトはフィールド変数とメソッドを持つ。
// このファイル内で汎用的に利用するメソッドも定義している。
// ==================================================



// ==================================================
// オブジェクトパネル
// ==================================================
var panel_object = function() {
	// html用ID
	this.id = "panel_object";
	
	
	// オブジェクトを描画
	this.view = function(obj) {
		dispic(obj.id, obj.bg_img, obj.x, obj.y, obj.kakudo);
	}
	
	// オブジェクト配列を描画
	this.view_array = function(obj_array) {
		for (var i = 0;i < obj_array.length; i++) {
			dispic(obj_array[i].id, obj_array[i].bg_img, obj_array[i].x, obj_array[i].y, obj_array[i].kakudo);
		}
	}
}

// ==================================================
// スコアパネル
// ==================================================
var panel_score = function(x, y) {
	// html用ID
	this.id = "panel_score";
	// X座標
	this.x = x + 45;
	// Y座標
	this.y = y + 25;
	// クラス名
	this.class_name = "panel_score";
	// ケタ数
	this.keta_su = 5;
	
	
	// 描画
	this.view = function(score) {
		dispic_txt(this.id, this.class_name, this.x, this.y, get_zeroume_str(score, this.keta_su));
	}
}

// ==================================================
// 時間パネル
// ==================================================
var panel_time = function(x, y) {
	// html用ID
	this.id = "panel_time";
	// X座標
	this.x = x + 80;
	// Y座標
	this.y = y + 25;
	// クラス名
	this.class_name = "panel_time";
	// ケタ数
	this.keta_su = 2;
	
	
	// 描画
	this.view = function(time) {
		// 表示用時間を生成
		var disp_time = String(Math.floor(time / 1000));
		
		dispic_txt(this.id, this.class_name, this.x, this.y, get_zeroume_str(disp_time, this.keta_su));
	}
}

// ==================================================
// ボタンパネル
// ==================================================
var panel_btn = function(x, y) {
	// html用ID
	this.id = "panel_btn";
	// X座標
	this.x = x;
	// Y座標
	this.y = y;
	// 背景画像
	this.bg_img = "./img/btn/btn.gif";
	// クラス名
	this.class_name = "panel_btn";
	
	
	// 描画
	this.view = function() {
		dispic(this.id, this.bg_img, this.x, this.y, 0);
	}
}

// ==================================================
// 結果パネル
// ==================================================
var panel_result = function(x, y) {
	// html用ID
	this.id = "panel_result";
	// X座標
	this.x = x;
	// Y座標
	this.y = y;
	// 背景画像
	this.bg_img = "./img/bg/result.jpg";
	
	
	// 描画
	this.view = function(fish) {
		dispic_for_result(this.id, this.bg_img, this.x, this.y, fish);
	}
}

// ==================================================
// 終了パネル
// ==================================================
var panel_end = function(x, y) {
	// html用ID
	this.id = "panel_end";
	// X座標
	this.x = x;
	// Y座標
	this.y = y;
	// 背景画像
	this.bg_img = "./img/bg/end.jpg";
	// クラス名
	this.class_name = "panel_end";
	
	
	// 描画
	this.view = function(score) {
		dispic_with_txt(this.id, this.bg_img, this.class_name, this.x, this.y, String(score));
	}
}

// ==================================================
// 投げなおし確認パネル
// ==================================================
var panel_recast = function(x, y) {
	// html用ID
	this.id = "panel_recast";
	// X座標
	this.x = x;
	// Y座標
	this.y = y;
	// メッセージ
	this.msg = "なげなおす？";
	// クラス名
	this.class_name = "panel_recast";
	
	
	// 描画
	this.view = function() {
		dispic_dialog(this.id, this.class_name, this.x, this.y, this.msg);
	}
}

// ==================================================
// 初回説明用パネル
// ==================================================
var panel_start = function(x, y) {
	// html用ID
	this.id = "panel_start";
	// X座標
	this.x = x;
	// Y座標
	this.y = y;
	// メッセージ
	this.msg = "どこかをクリックしてね。";
	// クラス名
	this.class_name = "panel_start";
	
	
	// 描画
	this.view = function() {
		dispic_dialog_one_button(this.id, this.class_name, this.x, this.y, this.msg);
	}
}

// ==================================================
// 指定位置に画像を表示
// ==================================================
function dispic(id, img_url, x, y, kakudo) {
	var image;
	
	if (document.getElementById(id) == null) {
		// オブジェクトが存在しない場合は新規作成
		image = document.createElement("img");
		image.setAttribute("id", id);
		image.setAttribute("src", img_url);
		image.style.position = "absolute";
		
		document.getElementById(NAME_ID_DISP).appendChild(image);
	} else {
		image = document.getElementById(id);
	}
	
	image.style.left = x + "px";
	image.style.top = y + "px";
	image.style.transform = "rotate(" + kakudo + "deg)";
}

// ==================================================
// 指定位置にテキスト表示
// ==================================================
function dispic_txt(id, class_name,  x, y, str) {
	var div;
	
	if (document.getElementById(id) == null) {
		// オブジェクトが存在しない場合は新規作成
		div = document.createElement("div");
		div.setAttribute("id", id);
		div.setAttribute("class", class_name);
		div.style.position = "absolute";
		div.style.left = x + "px";
		div.style.top = y + "px";
		
		document.getElementById(NAME_ID_DISP).appendChild(div);
	} else {
		div = document.getElementById(id);
	}
	
	div.innerText = str;
}

// ==================================================
// 指定位置にテキスト入り画像表示
// ==================================================
function dispic_with_txt(id, img_url, class_name,  x, y, str) {
	var div;
	
	if (document.getElementById(id) == null) {
		// オブジェクトが存在しない場合は新規作成
		div = document.createElement("div");
		div.setAttribute("id", id);
		div.setAttribute("class", class_name);
		div.style.backgroundImage = "url('" + img_url + "')";
		div.style.position = "absolute";
		div.style.left = x + "px";
		div.style.top = y + "px";
		
		// テキスト
		var div_text = document.createElement("div");
		div_text.setAttribute("class", class_name + "_text");
		div_text.innerText = str + " てん";
		div.appendChild(div_text);
		
		// ボタン
		var input = document.createElement("input");
		input.setAttribute("type", "button");
		input.setAttribute("value", "もういっかいやる");
		input.setAttribute("onclick", "on_click_btn_end();");
		input.setAttribute("class", "panel_end_button");
		div.appendChild(input);
		
		document.getElementById(NAME_ID_DISP).appendChild(div);
	} else {
		div = document.getElementById(id);
	}
}

// ==================================================
// 結果画面表示
// ==================================================
function dispic_for_result(id, img_url, x, y, fish) {
	var div;
	
	if (document.getElementById(id) == null) {
		// オブジェクトが存在しない場合は新規作成
		div = document.createElement("div");
		div.setAttribute("id", id);
		div.setAttribute("class", "panel_result");
		div.style.position = "absolute";
		div.style.backgroundImage = "url('" + img_url + "')";
		div.style.left = x + "px";
		div.style.top = y + "px";
		
		// 魚の画像
		var div_img = document.createElement("div");
		div_img.setAttribute("class", "panel_result_imgdiv");
		div.appendChild(div_img);
		
		var image = document.createElement("img");
		image.setAttribute("src", fish.result_img);
		image.setAttribute("class", "panel_result_img");
		div_img.appendChild(image);
		
		// 魚の名前
		var div_fishname = document.createElement("div");
		div_fishname.innerText = fish.name;
		div_fishname.setAttribute("class", "panel_result_fishname");
		div.appendChild(div_fishname);
		
		// 魚のスコア
		var div_fishscore = document.createElement("div");
		div_fishscore.innerText = fish.score + " てん";
		div_fishscore.setAttribute("class", "panel_result_fishscore");
		div.appendChild(div_fishscore);
		
		// ボタン
		var input = document.createElement("input");
		input.setAttribute("type", "button");
		input.setAttribute("value", "つづける");
		input.setAttribute("onclick", "on_click_btn_result();");
		input.setAttribute("class", "panel_result_button");
		div.appendChild(input);
		
		document.getElementById(NAME_ID_DISP).appendChild(div);
	} else {
		div = document.getElementById(id);
	}
}

// ==================================================
// 指定位置にダイアログ表示
// ==================================================
function dispic_dialog(id, class_name, x, y, str) {
	var div;
	
	if (document.getElementById(id) == null) {
		// オブジェクトが存在しない場合は新規作成
		div = document.createElement("div");
		div.setAttribute("id", id);
		div.setAttribute("class", class_name);
		div.style.position = "absolute";
		div.style.left = x + "px";
		div.style.top = y + "px";
		
		// テキスト
		var span = document.createElement("span");
		span.innerText = str;
		span.setAttribute("class", class_name + "_span");
		div.appendChild(span);
		
		// ボタン1
		var input = document.createElement("input");
		input.setAttribute("type", "button");
		input.setAttribute("value", "はい");
		input.setAttribute("onclick", "on_click_dialog_yes();");
		input.setAttribute("class", class_name + "_input");
		div.appendChild(input);
		
		// ボタン2
		var input2 = document.createElement("input");
		input2.setAttribute("type", "button");
		input2.setAttribute("value", "いいえ");
		input2.setAttribute("onclick", "on_click_dialog_no();");
		input2.setAttribute("class", class_name + "_input");
		div.appendChild(input2);
		
		document.getElementById(NAME_ID_DISP).appendChild(div);
	} else {
		div = document.getElementById(id);
	}
}

// ==================================================
// 指定位置にダイアログ表示
// ==================================================
function dispic_dialog_one_button(id, class_name, x, y, str) {
	var div;
	
	if (document.getElementById(id) == null) {
		// オブジェクトが存在しない場合は新規作成
		div = document.createElement("div");
		div.setAttribute("id", id);
		div.setAttribute("class", class_name);
		div.style.position = "absolute";
		div.style.left = x + "px";
		div.style.top = y + "px";
		
		// テキスト
		var span = document.createElement("span");
		span.innerText = str;
		span.setAttribute("class", class_name + "_span");
		div.appendChild(span);
		
		// ボタン1
		var input = document.createElement("input");
		input.setAttribute("type", "button");
		input.setAttribute("value", "はい");
		input.setAttribute("onclick", "on_click_dialog_start();");
		input.setAttribute("class", class_name + "_input");
		div.appendChild(input);
		
		document.getElementById(NAME_ID_DISP).appendChild(div);
	} else {
		div = document.getElementById(id);
	}
}

