// ==================================================
// システム
// 
// 
// ゲーム実行に関連するコードを定義する。
// 各オブジェクトはフィールド変数とメソッドを持つ。
// このファイル内で汎用的に利用するメソッドも定義している。
// ==================================================



// ==================================================
// システム
// ==================================================
var system = function(score, time, frame_time, player_power) {
	// スコア
	this.score = score;
	
	// 時間(ms)
	this.time = time;
	
	// 1フレームあたりの時間(ms)
	this.frame_time = frame_time;
	
	// フェーズ
	this.fase = new fase("BEFORE_CAST");
	
	// プレイヤー
	this.player = new player(player_power);
	
	// 魚の配列
	this.fish_array = new Array();
	
	// ウキ
	this.uki = new uki(this.player.x, this.player.y);
	
	// 初回説明用パネル
	this.panel_start = new panel_start(RANGE_WINDOW_WIDTH * 0.30, RANGE_WINDOW_HEIGHT * 0.40);
	// オブジェクトパネル
	this.panel_object = new panel_object();
	// スコアパネル
	this.panel_score = new panel_score(RANGE_WINDOW_WIDTH * 0.05, RANGE_WINDOW_HEIGHT  * 0.05);
	// 時間パネル
	this.panel_time = new panel_time(RANGE_WINDOW_WIDTH * 0.75, RANGE_WINDOW_HEIGHT  * 0.05);
	// ボタンパネル
	this.panel_btn = new panel_btn(RANGE_WINDOW_WIDTH * 0.70, RANGE_WINDOW_HEIGHT * 0.5);
	// 結果パネル
	this.panel_result = new panel_result(RANGE_WINDOW_WIDTH * 0.30, RANGE_WINDOW_HEIGHT * 0.25);
	// 終了パネル
	this.panel_end = new panel_end(RANGE_WINDOW_WIDTH * 0.30, RANGE_WINDOW_HEIGHT * 0.25);
	// 投げなおし確認パネル
	this.panel_recast = new panel_recast(RANGE_WINDOW_WIDTH * 0.30, RANGE_WINDOW_HEIGHT * 0.40);
	
	
	// 魚を設定
	this.set_fish_array = function(fish_array) {
		this.fish_array = fish_array;
	}
	
	// スタート時の処理
	this.start = function() {
		this.panel_start.view();
	}
	
	// 更新
	this.update = function() {
		// クリックによる更新
		if (true == CLICK_INFO.is_click) {
			this.player.update_by_click(this.fase, this.uki, this.fish_array, CLICK_INFO.x, CLICK_INFO.y);
			
			CLICK_INFO.reset();
		}
		
		// ユーザーによりポーズされた場合、以降の処理は実施しない
		if (true == IS_PAUSE) {
			return;
		}
		
		// 全ての魚を更新
		for (var i = 0;i < this.fish_array.length; i++) {
			this.fish_array[i].update(this.fase, this.uki);
		}
		
		// ウキを更新
		this.uki.update(this.fase);
		
		// 釣られた魚がいる場合はスコアを加算
		var get_fish = get_is_get_fish(this.fish_array);
		if (null != get_fish) {
			this.score = this.score + get_fish.score;
		}
		
		// 時間を進める
		this.time = this.time - this.frame_time;
		
		// 時間切れになった場合
		if (this.time < 0) {
			this.fase.value = "END";
		}
	}
	
	// 描画
	this.view = function () {
		// 全てのパネルを削除
		this.delete_all_panel();
		
		switch (this.fase.value) {
			case "BEFORE_CAST":
				this.panel_score.view(this.score);
				this.panel_time.view(this.time);
				this.panel_object.view_array(this.fish_array);
				
				break;
			case "AFTER_CAST":
				this.panel_score.view(this.score);
				this.panel_time.view(this.time);
				this.panel_object.view_array(this.fish_array);
				this.panel_object.view(this.uki);
				
				break;
			case "DIALOG_RECAST":
				this.panel_score.view(this.score);
				this.panel_time.view(this.time);
				this.panel_object.view_array(this.fish_array);
				this.panel_object.view(this.uki);
				this.panel_recast.view();
				
				break;
			case "FIGHTING":
				this.panel_score.view(this.score);
				this.panel_time.view(this.time);
				this.panel_object.view_array(this.fish_array);
				this.panel_object.view(this.uki);
				this.panel_btn.view();
				
				break;
			case "RESULT":
				this.panel_score.view(this.score);
				this.panel_time.view(this.time);
				this.panel_object.view_array(this.fish_array);
				this.panel_result.view(get_is_get_fish(this.fish_array));
				
				IS_PAUSE = true;
				
				break;
			case "END":
				this.panel_end.view(this.score);
				
				IS_PAUSE = true;
				
				break;
			default:
				// 何もしない
		}
	}
	
	// 終わりの処理
	this.finalize = function () {
		// 釣られた魚がいる場合はその魚を削除
		var get_fish = get_is_get_fish(this.fish_array);
		if (null != get_fish) {
			delete_system_element(get_fish.id, this.fish_array);
			delete_view_element(get_fish.id);
		}
	}
	
	// 全パネル削除
	this.delete_all_panel = function () {
		delete_view_element(this.panel_start.id);
		delete_view_element(this.panel_object.id);
		delete_view_element(this.uki.id);
		delete_view_element(this.panel_score.id);
		delete_view_element(this.panel_time.id);
		delete_view_element(this.panel_btn.id);
		delete_view_element(this.panel_result.id);
		delete_view_element(this.panel_end.id);
		delete_view_element(this.panel_recast.id);
	}
}

// ==================================================
// フェーズ
// ==================================================
var fase = function(fase) {
	this.value = fase;
}

// ==================================================
// クリック情報
// ==================================================
var click_info = function() {
	// クリックしたか
	this.is_click = false;
	// X座標
	this.x = 0;
	// Y座標
	this.y = 0;
	
	// 設定
	this.set_value = function(is_click, x, y) {
		this.is_click = is_click;
		this.x = x;
		this.y = y;
	}
	
	// リセット
	this.reset = function() {
		this.is_click = false;
		this.x = 0;
		this.y = 0;
	}
}

